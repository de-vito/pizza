import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';

export const Groups = new Mongo.Collection('groups');
export const Users = Meteor.users;


if(Meteor.isServer){
 Meteor.publish('ingroup',function(id){
	check(id, String);
	return Groups.find({_id: id})
});

Meteor.publish('groups',function(){
	return Groups.find()
});
Meteor.publish('users',function(){
	return Users.find()
});
};
Meteor.methods({
  'creategroup'(name){
    check(name,String);
    if (! this.userId){
      throw new Meteor.Error('not-authorized');
    }
    Groups.insert({
      groupname :name,
      admin : this.userId,
      users:[],
      menu:[]

    });
  },
  'addtomenu'(id,item){

    Groups.update({_id : id },{$push:{menu:item}});
  }
})
