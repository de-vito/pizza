import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

Accounts.onLogin(function(){
  FlowRouter.go('mainpage')
});
Accounts.onLogout(function(){
  FlowRouter.go('home')
});



FlowRouter.route('/',{
  name:'home',
  action(){
    if(Meteor.userId()){
      FlowRouter.go('mainpage')
    }
    BlazeLayout.render('HomeLayout');
  }
});

FlowRouter.route('/main',{
  name: 'mainpage',
  action(){
    BlazeLayout.render('MainLayout',{main: 'mainPage'})
  }
});

FlowRouter.route('/addgroup',{
  name :'addgroup',
  action(){
    BlazeLayout.render('MainLayout',{main:'createGroup'})
  }
});

FlowRouter.route('/group/:id',{
	name: 'group',
	action(){
		BlazeLayout.render('MainLayout', {main: 'group'});
	}
});

FlowRouter.route('/group/addusers/:id',{
  name: 'addusers',
  action(){
    BlazeLayout.render('MainLayout', {main:'addUsers'})
  }
});
FlowRouter.route('/group/editmenu/:id',{
  name :'editmenu',
  action(){
    BlazeLayout.render('MainLayout',{main :'menu'})
  }
});
