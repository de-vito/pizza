
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import {Groups} from '../../../api/collections.js';
import '../../../api/collections.js';
import './menu.html';

Template.menu.onCreated(function () {
	var self = this;
	self.autorun(function () {
		var id= FlowRouter.getParam('id');
	  self.subscribe('ingroup',id);
    console.log(id)
	})
});



Template.menu.events({
  'click #addtomenu':function(e,tmpl){
    e.preventDefault;
    var itemName =tmpl.find('#menuitem').value;
    var itemPrice = tmpl.find('#itemprice').value;
    var id = FlowRouter.getParam('id');
		var item = {
			item : itemName,
			price : itemPrice
		};
		Meteor.call('addtomenu',id,item);
		tmpl.find('#menuitem').value="";
		tmpl.find('#itemprice').value="";

    console.log(item);
		// Meteor.call('addtomenu',id,item,price)
    // Groups.update({_id : "id"}, {$push: {menu: "item: 'item' , price: 'price'"}})
  }
})
