import { Template } from 'meteor/templating';

import './createGroup.html';

import { Groups } from '../../../api/collections.js';

Template.createGroup.events({
	'click #create-new-group' :function (e, tmpl) {
		e.preventDefault();
		var name = tmpl.find('#name-of-new-group').value;
		// var adminId  = Meteor.userId();
		Meteor.call('creategroup',name)
		// Groups.insert({groupname : groupName, admin :adminId, users :[], menu :[]});
		FlowRouter.go('mainpage');
	}

});
