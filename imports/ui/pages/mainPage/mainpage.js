import './mainpage.html';
import {Groups} from '../../../api/collections.js';


Template.mainPage.onCreated(function () {
	var self = this;
	self.autorun(function () {
		self.subscribe('groups')
	})
});


Template.mainPage.helpers({
isAdmin: function(){
  return this.admin === Meteor.userId();
},
groups:function(){
    return Groups.find();
  }
})
