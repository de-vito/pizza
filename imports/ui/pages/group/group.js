import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import './group.html';

import {Groups} from '../../../api/collections.js';




Template.group.onCreated(function () {
	var self = this;
	self.autorun(function () {
		var id= FlowRouter.getParam('id');
		self.subscribe('ingroup',id);
    console.log(id)
	})
});

Template.group.helpers({
	singlegroup: ()=> {
		var id= FlowRouter.getParam('id');
		return Groups.findOne({_id : id});
	}
});
