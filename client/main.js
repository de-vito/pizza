import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
// import { Groups } from '../imports/api/collections.js';

import '../imports/startup/client/index.js';
import '../imports/ui/layouts/homeLayout.js';
import '../imports/ui/layouts/mainLayout.js';
import '../imports/ui/components/header/header.js';
import '../imports/ui/pages/createGroup/createGroup.js';
import '../imports/ui/pages/mainPage/mainpage.js';
import '../imports/ui/pages/group/group.js';
import '../imports/ui/pages/addusers/addusers.js';
import '../imports/ui/pages/menu/menu.js'
